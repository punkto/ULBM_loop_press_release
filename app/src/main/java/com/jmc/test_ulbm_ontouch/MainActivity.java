package com.jmc.test_ulbm_ontouch;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity implements MediaPlayer.OnCompletionListener {

    private final int TRACKS = 3;

    private TextView textDebug;
    private ToggleButton vibratorToggle;
    private Button[] save_buttons = new Button[TRACKS];
    private Button[] play_buttons = new Button[TRACKS];

    private boolean[] track_is_playing = new boolean[TRACKS];

    private boolean vibratorIsActive = true;
    private boolean isExternalStorageAvailable = false;

    private String file_name[] = new String[TRACKS];

    private MediaRecorder recorder[] = new MediaRecorder[TRACKS];
    private MediaPlayer player[] = new MediaPlayer[TRACKS];
    private File recordedFile[] = new File[TRACKS];
    private Vibrator vibe;

    private char[] comenzar_grabacion = {'C','o','m','e','n','z','a','r',' ','g','r','a','b','a','c','i','ó','n'}; //len 18
    private char[] parar_grabacion = {'P','a','r','a','r',' ','g','r','a','b','a','c','i','ó','n'}; // len 15
    private char[] comenzar_reproduccion = {'C','o','m','e','n','z','a','r',' ','r','e','p','r','o','d','u','c','c','i','o','n'}; // len 21
    private char[] parar_reproduccion = {'P','a','r','a','r',' ','r','e','p','r','o','d','u','c','c','i','o','n'}; // len 18

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textDebug=(TextView)findViewById(R.id.textDebug);

        //TODO: put this in the for loop
        save_buttons[0] = (Button)findViewById(R.id.b_save_1);
        save_buttons[0].setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    textDebug.setText("save_1 pressed");
                    start_save_track(0);
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    textDebug.setText("save_1 released");
                    stop_save_track(0);
                    return true;
                }
                return true;
            }
        });

        play_buttons[0] = (Button)findViewById(R.id.b_play_1);

        save_buttons[1] = (Button)findViewById(R.id.b_save_2);
        save_buttons[1].setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    textDebug.setText("save_2 pressed");
                    start_save_track(1);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    textDebug.setText("save_2 released");
                    stop_save_track(1);
                }
                return true;
            }
        });

        play_buttons[1] = (Button)findViewById(R.id.b_play_2);

        save_buttons[2] = (Button)findViewById(R.id.b_save_3);
        save_buttons[2].setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    textDebug.setText("save_3 pressed");
                    start_save_track(2);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    textDebug.setText("save_3 released");
                    stop_save_track(2);
                }
                return true;
            }
        });

        play_buttons[2] = (Button)findViewById(R.id.b_play_3);

        for (int i = 0; i < TRACKS; i++){
            play_buttons[i].setEnabled(false);

            track_is_playing[i] = false;

            file_name[i] = new String("temp"+ String.valueOf(i));

            recorder[i] = new MediaRecorder();
        }

        vibratorToggle = (ToggleButton)findViewById(R.id.toggleButton);
        vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            isExternalStorageAvailable = true;
        }
    }


    private void start_save_track (int track){
        disableAllButtons();

        recorder[track].setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder[track].setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder[track].setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        if (isExternalStorageAvailable == false) {
            try {
                recordedFile[track] = File.createTempFile(file_name[track], ".3gp");
            } catch (IOException e) {
            }
        } else {
            File path = new File(Environment.getExternalStorageDirectory()
                    .getPath());
            try {
                recordedFile[track] = File.createTempFile(file_name[track], ".3gp", path);
            } catch (IOException e) {
            }
        }

        recorder[track].setOutputFile(recordedFile[track].getAbsolutePath());

        try {
            recorder[track].prepare();
        } catch (IOException e) {
        }
        recorder[track].start();

        textDebug.setText("Grabando");
        save_buttons[track].setText(parar_grabacion,0,15);

        if (vibratorIsActive == true) {
            vibe.vibrate(1000000);
        }
        save_buttons[track].setEnabled(true);
    }

    private void stop_save_track (int track){
        save_buttons[track].setEnabled(false);
        try {
            recorder[track].stop();
        } catch (Exception e){

        }
        recorder[track].reset();
        player[track] = new MediaPlayer();
        player[track].setOnCompletionListener(this);
        try {
            player[track].setDataSource(recordedFile[track].getAbsolutePath());
        } catch (IOException e) {
        }
        try {
            player[track].prepare();
        } catch (IOException e) {
        }
        save_buttons[track].setText(comenzar_grabacion,0,15);
        textDebug.setText("Listo para reproducir");
        if (vibratorIsActive == true) {
            vibe.cancel();
        }
        enableAllButtons();
    }

    public void play_1(View view) {
        textDebug.setText("play_1 pushed");
        play_track(0);
    }

    public void play_2(View view) {
        textDebug.setText("play_2 pushed");
        play_track(1);
    }

    public void play_3(View view) {
        textDebug.setText("play_3 pushed");
        play_track(2);
    }

    private void play_track (int track){
        if (track_is_playing[track] == false) {
            disableAllButtons();
            track_is_playing[track] = true;
            play_buttons[track].setText(parar_reproduccion,0,18);
            player[track].start();
            textDebug.setText("Reproduciendo");
            if (vibratorIsActive == true) {
                vibe.vibrate(1000000);
            }
            enableAllButtons();
            save_buttons[track].setEnabled(false);
        } else {
            disableAllButtons();
            track_is_playing[track] = false;
            play_buttons[track].setText(comenzar_reproduccion,0,21);
            player[track].stop();
            if (vibratorIsActive == true) {
                vibe.cancel();
            }
            try {
                player[track].prepare();
            } catch (IOException e) {
            }
            enableAllButtons();
        }
    }

    private int whichTrackIsPlaying(){
        for (int i = 0; i < TRACKS; i++){
            if (track_is_playing[i] == true){
                return i;
            }
        }
        return -1;
    }

    public void onCompletion(MediaPlayer mp) {
        int track = whichTrackIsPlaying();
        if (track == -1) return;

        track_is_playing[track] = false;
        textDebug.setText("Listo");
        play_buttons[track].setText(comenzar_reproduccion,0,21);
        if (vibratorIsActive == true) {
            vibe.cancel();
        }
        save_buttons[track].setEnabled(true);
    }


    public void toggleVibrator(View view){
        if (vibratorIsActive == true) {
            vibratorIsActive = false;
        } else {
            vibratorIsActive = true;
        }
    }

    private void disableAllButtons(){
        for (Button b: play_buttons){
            b.setEnabled(false);
        }
        for (Button b: save_buttons){
            b.setEnabled(false);
        }
    }

    private void enableAllButtons(){
        for (int i = 0; i < TRACKS; i++){
            if (recorder[i] != null) {
                play_buttons[i].setEnabled(true);
            }
        }
        for (Button b: save_buttons){
            b.setEnabled(true);
        }
    }

    private void enableSaveButtons(){
        for (Button b: save_buttons){
            b.setEnabled(true);
        }
    }
}
